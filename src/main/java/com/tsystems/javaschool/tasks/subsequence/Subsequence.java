package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException("Input lists are null!");
        }

        List yCopy = new LinkedList(y);
        Iterator xIt = x.iterator();
        while (xIt.hasNext()) {
            Object xElem = xIt.next();
            int i = yCopy.indexOf(xElem);
            if (i == -1) {
                return false;
            }
            if (i == yCopy.size()-1 && xIt.hasNext()) {
                return false;
            }
            yCopy = yCopy.subList(i+1, yCopy.size());
        }

        return true;
    }
}
