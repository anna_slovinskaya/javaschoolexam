package com.tsystems.javaschool.tasks.pyramid;

import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers == null || inputNumbers.isEmpty() || inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        int inputSize = inputNumbers.size();
        double sqrt = (Math.sqrt(8*inputSize+1));
        if (Double.isInfinite(sqrt) || Double.isNaN(sqrt) || sqrt != Math.floor(sqrt)) {
            throw new CannotBuildPyramidException();
        }

        inputNumbers.sort((i, j) -> i-j);
        int linesNum = ((int)sqrt - 1)/2;
        int[][] res =  new int[linesNum][2*linesNum-1];

        int firstIndex = (2*linesNum-1)/2;
        int lastIndex = firstIndex;
        int index = firstIndex;

        Iterator<Integer> iterator = inputNumbers.iterator();
        for (int line = 0; line < res.length; line++) {
            while (index <= lastIndex) {
                res[line][index] = iterator.next();
                index = index + 2;
            }
            firstIndex--;
            lastIndex++;
            index = firstIndex;
        }

        return res;
    }
}
