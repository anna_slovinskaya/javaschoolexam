package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {

    private enum Operator {
        MULT('*'),
        DIV('/'),
        ADD('+'),
        SUB('-');

        private char value;
        Operator(char value)
        {
            this.value = value;
        }

        public String toString()
        {
            return String.valueOf(value);
        }
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {
        if (statement == null || statement.isEmpty()) {
            return null;
        }
        if (hasNegativeVals(statement)) {
            return null;   
        }

        try {
            StringBuffer sb = new StringBuffer(statement);
            String res = evaluate(sb);
            if (res == null) {
                return null;
            }

            BigDecimal bd = new BigDecimal(res);
            bd = bd.stripTrailingZeros();
            if (bd.scale() <= 4) {
                return bd.toPlainString();
            }
            bd = bd.setScale(4, RoundingMode.HALF_UP).stripTrailingZeros();
            return bd.toPlainString();
        } catch (RuntimeException e) {
            return null;
        }
    }

    private boolean hasNegativeVals(String statement) {
        for (Operator o : Operator.values()) {
            if (statement.contains(new StringBuilder().append(o).append(Operator.SUB))) {
                return true;
            }
        }
        return false;
    }

    private String evaluate(StringBuffer sb) {
        int firstBracketIndex = sb.indexOf("(");
        if (firstBracketIndex != -1) {
            int secondBracketIndex = sb.lastIndexOf(")");
            if (secondBracketIndex == -1) {
                return null;
            }
            StringBuffer newSB = new StringBuffer(sb.subSequence(firstBracketIndex + 1, secondBracketIndex));
            String evaluated = evaluate(newSB);
            sb.replace(firstBracketIndex, secondBracketIndex + 1, evaluated);
        }

        evaluateSigns(sb, Operator.MULT, Operator.DIV);
        evaluateSigns(sb, Operator.ADD, Operator.SUB);
        return sb.toString();
    }

    private void evaluateSigns(StringBuffer sb, Operator... signs) {
        String doubleRegEx = "((-?)(0|([1-9][0-9]*))(\\.[0-9]+)?)";
        StringBuilder operationRegEx = new StringBuilder();
        operationRegEx.append('(').append(doubleRegEx).append('(');
        for (int i = 0; i < signs.length; i++) {
            operationRegEx.append('\\').append(signs[i]);
            if (i != signs.length-1) {
                operationRegEx.append('|');
            }
        }
        operationRegEx.append(')').append(doubleRegEx).append(')');
        Pattern pattern = Pattern.compile(operationRegEx.toString());

        Matcher matcher = pattern.matcher(sb.toString());

        sb.setLength(0);
        while (matcher.find()) {
            try {
                BigDecimal result;
                BigDecimal bd1 = new BigDecimal(matcher.group(2));
                BigDecimal bd2 = new BigDecimal(matcher.group(8));
                switch (matcher.group(7)) {
                    case "*":
                        result = bd1.multiply(bd2);
                        break;
                    case "/":
                        result = bd1.divide(bd2, 8, RoundingMode.HALF_UP);
                        break;
                    case "+":
                        result = bd1.add(bd2);
                        break;
                    case "-":
                        result = bd1.subtract(bd2);
                        break;
                    default:
                        throw new NumberFormatException("Illegal sign!");
                }
                String s = matcher.replaceFirst(result.toString());
                matcher = pattern.matcher(s);
            } catch (NumberFormatException | ArithmeticException e) {
                throw new IllegalArgumentException("Incorrect format!", e);
            }
        }
        matcher.appendTail(sb);
    }
}